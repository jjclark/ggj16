require(['src/util/domUtils'], function(dom){

	var changed={};
	var restore;

	function getUserName(){
		var name = localstorage.getItem('britual.username');
		if(!name){
			prompt("Hello Britual Admin! I'm so sorry to interrupt, but it seems I don't know your name. Would you be so kind...?")
		}
	}

	function getData(callback){
		$.getJSON('/data', callback, function(){},'json')
	}

	function postData(id, data) {
		var count =Object.keys(changed).length;
		if(count>0) {
			//$.post('/data', JSON.stringify(changed));
			$.ajax({
		        url : "/data",
		        type: "POST",
		        data: JSON.stringify(changed),
		        contentType: "application/json; charset=utf-8",
		        dataType   : "json",
		        success    : function(){
		            console.log("Pure jQuery Pure JS object");
		        }
		    });
		}
		
	}

	function addContent(){
		var id = prompt("It would be my absolute pleasure to add a new Scenario for you. Only I don't know what you want me to call it. Sorry to keep on with the questions, but would you mind giving me an id?")
		var item = changed[id]=window.data.scenarios[id]={ title: "", content:"Enter text..."};
		updateSave()
		updateList(window.data);
		renderItem(item,id);
	}

	function removeContent(){
		if(window._selected) {
			// remove the selected item
			var id = window._selected.id;
			window.data.scenarios[id]=null;
			changed[id]=null;
			updateSave();
			//markItemChanged(window._selected);
			updateList(window.data);
			renderItem();
		}else {
			alert("Oh my! Er, you have to select some content before you can remove it");
		}
	}

	function updateList(data) {
		var items = data.scenarios;
		var list = $('#scenarioList')[0];
		list.innerHTML="";
		for(var id in items) {
			var s = items[id];
			if(s){
				// watch out for special cases like the prologue, which is multi-part
				if(s.concat && s.splice){
					for(var i=0;i<s.length;i++) {
						renderListItem(list,s[i],"prologue");
					}
				} else {
					renderListItem(list, s, id);
				}
			}
		}
	}

	function renderListItem(parent, data, title){
		var id = title||data.title
		if(!data.id) data.id = id;
		var label = id;
		if(changed[id]) {
			label+="*"
		}
		var node = dom.create({type:'li',parent:parent, text:label, click:function(){
			renderItem(data, id);
		}})
		node.data=data;
		return node;	
	}

	function renderItem(data, id){
		var root = $('#formRoot')[0];
		root.innerHTML="";

		window._selected = data;
		if(!data) return;

		// show some indication that there are unsaved changed
		if(changed[id]) {
			dom.create({parent:root,text:"Oh, I just thought you ought to know, it's nothing really, but it's just that this page has unsaved changes...",class:"save-warning"});
		}

		_createField(root,'ID', id, function(){},{readonly:true})
		_createField(root,'Title',  data.title, function(text){
			data.title=text;
			markItemChanged(data);
		})
		_createField(root,'Content',data.content, function(text){
			data.content=text;
			markItemChanged(data);
		},{richText:true})
		_createField(root,'Image',data.image||"", function(text){
			data.image=text;
			markItemChanged(data);
		});

		dom.create({parent:root, text: "Options", class:"subtitle"});

		dom.create({type:"span",parent:root,class:"button",text:"Add Option",click:function(){
			var n = {content:"Enter option..."};
			if(!data.options) {data.options=[]}
			data.options.push(n);
			renderItem(data,id);
			markItemChanged(data);
		}});
		
		var optionWrapper=dom.create({parent:root,class:"subsection"});
		var opts = data.options;
		if(opts){
			opts.forEach(function(opt,i) {
				var optRoot = _createGroupItem(optionWrapper, opt, "Option #"+i,function(newValue){
					opt.content=newValue;
					markItemChanged(data)
				},function(){
					opts.splice(i,1);
					markItemChanged(data);
					// redraw the whole group after the remove, else I'll have sync issues
					renderItem(data,id);
				});
				if(!opt.results) opt.results={};
				_generateDecisionTable(optRoot,opt.results);
			});
		}

		dom.create({parent:root, text: "Resolutions", class:"subtitle"});

		var resWrapper=dom.create({parent:root,class:"subsection"});
		var res = data.resolutions;
		dom.create({type:"span",parent:root,class:"button",text:"Add Resolution",click:function(){
			var n = {content:"Enter resolution..."};
			if(!data.resolutions) {data.resolutions=[]}
			data.resolutions.push(n);
			renderItem(data,id);
			markItemChanged(data);
		}});
		if(res){
			res.forEach(function(r,i){
				var node = _createGroupItem(resWrapper, r, "Resolution #"+i,function(newValue){
					r.content=newValue;
					markItemChanged(data)
				},function(){
					res.splice(i,1);
					markItemChanged(data);
					// redraw the whole group after the remove, else I'll have sync issues
					renderItem(data,id);
				});
				var wrapper = dom.create({parent:node});
				_createField(wrapper, "World Type", r.env||"", function(text){
					r.env=text;
					markItemChanged(data);
				},{inline: true})
        		_createField(wrapper, "Result (good/ok/bad)", r.result||"",function(text){
        			r.result=text;
					markItemChanged(data);
        		},{inline: true});
			});
		}

	}

	function _generateDecisionTable(parent, data){
		var root = dom.create({type:"table",parent:parent});
		var worlds = dom.create({type:"tr", parent: root});
		var r1 = dom.create({type:"th", parent: worlds, text:"Normal"});
		var r2 = dom.create({type:"th", parent: worlds, text:"Aggressive"});
		var r3 = dom.create({type:"th", parent: worlds, text:"Surreal"});

		var results = dom.create({type:"tr", parent: root});
		var r1 = dom.create({type:"td", parent: results});
		_getSelect(r1,data.normal,function(x){
			data.normal=x;
		});
		var r2 = dom.create({type:"td", parent: results});
		_getSelect(r2,data.aggressive,function(x){
			data.aggressive=x;
		});
		var r3 = dom.create({type:"td", parent: results});
		_getSelect(r3, data.surreal,function(x){
			data.surreal=x;
		});
	}

	function _getSelect(parent, def, onchange){
		var opts=["bad", "ok", "good"];
		var select = dom.create({type:"select", parent:parent});
		var idx,val,i=0;
		while(opts[0]) {
			val = opts.shift();
			if(val==def){
				idx=i;
			}
			i++;
			dom.create({type:"option",text:val,parent:select});
		}
		select.selectedIndex = idx;
		$(select).on('change', function(){
			var sel = this.item(this.selectedIndex)
			onchange(sel.textContent);
			markItemChanged();
		})

	}

	function markItemChanged(item){
		if(!item) {
			item = window._selected;
		}
		changed[item.id]=item;
		updateSave();
	}

	function updateSave(){
		var count =Object.keys(changed).length;
		// only do this if the count has changed
		$('#saveButton').text("Save ("+count+")");
		updateList(window.data)
	}

	function restore(){
		var data = JSON.parse(restore);
		window.data = data;
		updateList(data);
	}

	function _createGroupItem(parent, data, idx, onChange, onRemove){
		var wrapper = dom.create({parent:parent,class:"subsection-group"});
		var header=dom.create({class:"group-toolbar",parent:wrapper})
		var removectrl = dom.create({type:"span",parent:header,text:"Remove",click:onRemove,class:"button"});
		dom.create({type:"span",parent:header,text: idx})
		var editor = dom.create({parent:wrapper,html:data.content});
		var prev = data.content;
		$(editor).keyup(function(e) {
	 		if(editor.innerHTML!=prev) {
	 			onChange(editor.innerHTML);
	 		}
		 });
		var easyEditor = new EasyEditor(editor, {
            buttons: []
        });
        return wrapper;
	}

	function _createField(parent, label, value, onChange, opts){
		opts=opts||{};
		var wrapper = dom.create({parent:parent, class:'field-wrapper'});
		if(opts.inline){
			wrapper.className+=" inline";
		}
		var labelNode = dom.create({text:label,parent:wrapper});
		var prev=value;
		if(opts.readonly){
			dom.create({parent:wrapper, text:value});
		}
		else if(opts.richText) {
			var editor = dom.create({parent:wrapper,html:value});
			 $(editor).keyup(function(e) {
		 		if(editor.innerHTML!=prev) {
		 			prev=editor.innerHTML;
		 			onChange(editor.innerHTML);
		 		}
			 });
			var easyEditor = new EasyEditor(editor, {
	            buttons: ['bold', 'italic', 'link', 'h2', 'h3', 'h4', 'quote', 'image', 'list', 'x']
	        });
		} else {
			var inputNode = dom.create({type:'input',parent:wrapper, change:onChange});
			$(inputNode).keyup(function(e) {
		 		if(inputNode.value!=prev) {
		 			prev=inputNode.value;
		 			onChange(inputNode.value);
		 		}
			 });
			inputNode.value = value;
		}
		return wrapper;
	}


	// init the page
	getData(function(d){
		window.data=d;
		restore=JSON.stringify(d);
		updateList(d);
	})

	$('#saveButton').click(function(){
		postData()
		changed={};
		updateSave();
	});

	$('#restoreButton').click(function(){
		restore();
	});

	$('#addButton').click(function(){
		addContent();
	});

	$('#removeButton').click(function(){
		removeContent();
	});

});