displaySplash();

require(['src/brituals'], function(Brituals){

	var game = new Brituals($('#canvas')[0]);
	game.generateCredits();

	$('#beginBtn').click(function(){
		game.step();

		$('#beginBtn').remove()
	});

	$('#randomBtn').click(function(){
		game._loadRandom();
	})

	$('#interBtn').click(function(){
		game._showIntermission();
	})

	window.scenario = function(id){
		game.loadScenario(id);
	}

})

var splashOpen;
function displaySplash(){
	$('#splash1').addClass("show");
	
	splashOpen=1;
	setTimeout(function(){
		if(splashOpen) {
			dismissSplash();
		}
	}, 5000)
};

function dismissSplash(){
	if(splashOpen==1) {
		$('#splash1').removeClass("show");
		splashOpen=2;
		$('#splash2').addClass("show");
	}else if(splashOpen==2) {
		$('#splash2').removeClass("show");
	}
};


