define(['src/util/domUtils'], function(dom){

	var BritualsGame = function(canvasNode, opts){
		this.nodes={
			canvas: canvasNode
		};
		this.state={
			turn: -1
		};
		this.startup();
	};

	BritualsGame.prototype={

		startup: function(){
			console.log("Dragging Britual client from the eldrich depths of the abyss")
			this.loadGameData(function(data){
				// trigger the first step
				this.gameData = data;
				// tmp
				this.step();
			}.bind(this));
		},

		// load game data
		loadGameData: function(done){
			$.getJSON('src/data.json', function(data){
				console.log("game json loaded!");
				done(data);
			})
		},

		// render a page on screen
		render: function(data){
			if(typeof data=="string"){
				// if we get a string, it's just a page resoltion.
				data = {
					content:data,
					options:[{
						content: "Continue..."
					}]
				}
			}
			var node = this.nodes.canvas;
			this.currentPage=data;
			node.innerHTML="";

			if(data.intermission) {
				// build the page
				node.appendChild(this._renderIntermission(node, data.content));
			}else {
				node.appendChild(this._renderScenario(node, data.content, data.options));
			}

			// load options
		},

		_renderScenario: function(node, content, options){
			var wrapper = dom.create({});
			var body = dom.create({parent:wrapper, html:content});
			if(options){
				var optionRoot = dom.create({type:'ul', parent:body});
				var self=this;
				options.forEach(function(d){
					var n = dom.create({type:'li',parent:optionRoot, html:d.content, click:function(){
						var option = this.getAttribute('data-opt')
						self.step(option)
					}})
					if(d.result) {
						n.setAttribute('data-opt', d.result);
					}
				});
			}
			return wrapper;
		},

		_renderIntermission: function(node, content){
			var wrapper = dom.create({});
			var body = dom.create({parent:wrapper, html:content});
			var continueRoot = dom.create({type:'ul', parent:body});
			// add some kind of loading throbber
			var self=this;
			dom.create({type:'li',parent:continueRoot, text:"continue", click:function(){
				self.step()
			}})
			return wrapper;

		},


		// step through the game's ticks
		// this is the main game loop
		// turn one is the prologue, the last turn is the epilogue
		// every other turn loads a random scenario
		// each turn has a setup and a resolution
		step: function(option){
			var state = this.state;
			if(option) {
				// same turn, just figure out the resolution
				console.log("completing turn"+state.turn+" with resut: "+option)
				this.render(this.currentPage.resolutions[option])

			}else {
				// load the next turn
				state.turn++;
				console.log("Stepping to turn "+state.turn)

				if(this.hasPrologue()){
					this.render(this._getPrologue());
				}else {
					// render an intermission, then a scenario
					if(state.intermission){
						state.intermission = false;
						this.render(this._getScenario());
					}else {
						state.intermission = true;
						this.render(this._getIntermission());
					}
				}
			}

		},

		hasPrologue: function(){
			return this.gameData.scenarios.prologue.length>0;
		},

		// we may have multiple prologues. Consider a shift() to get rid of them
		_getPrologue: function(){
			return this.gameData.scenarios.prologue.shift();
		},

		_getScenario: function(){
			console.log("RANDOM SCENARIO");
			return {
				"title":"Page2",
				"content": "RANDOM SCENARIO! Ask the writer.",
				"options": [
					{ "content": "Opt 1", "result":"good" },
					{ "content": "Opt2", "result":"indifferent" },
					{ "content": "Opt2", "result":"bad" }
				],
				"resolutions":{

					"good": "x",
					"indifferent": "x",
					"bad": "x"
				}
			}
		},

		_getIntermission:function(){
			return {
				content:this.gameData.intermissions[0],
				intermission: true
			}
		}


	};

	return BritualsGame;


})