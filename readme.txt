BRITUAL

:: A cross-dimensional social awkwardness simulacrum

Britual runs in a browser using a custom game engine - basically Twine but without the Twine.

It relies on a backend server to run. You'll need node.js installed. From the /server folder, run:

	npm install

And then

	node server

And pray to your favourite God, Gods or Power Ranger.

In the browser, navigate to localhost:8080/ to play the game.

From localhost:8080/admin, you can access the backend CMS to add or edit scenarios. It's all a bit flaky. The front-end engine makes assumptions about the prologue and epilogue pages, so mess with them at your peril.

Thanks and have fun!

Joe @jjc-Uk
JammersInPyjammers
