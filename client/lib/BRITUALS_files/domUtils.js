define([], function() {

    return {
        // centralise referneces to model names
        models: {
            LINK: "link",
            PAGE: "page",
            CONTENT: "content"
        },
        create: function(kwArgs) {
            if (arguments.length > 1) {
                return this._oldCreate.apply(this, arguments);
            } else {
                var dom = document.createElement(kwArgs.type || "div");
                if (kwArgs.class) {
                    dom.className = kwArgs.class;
                }
                if (kwArgs.id) {
                    dom.setAttribute("id", kwArgs.id);
                }
                var attrs = kwArgs.attrs;
                if (attrs) {
                    for (var a in attrs) {
                        dom.setAttribute(a, attrs[a]);
                    }
                }
                if (kwArgs.text) {
                    dom.textContent = kwArgs.text;
                } else if (kwArgs.html) {
                    dom.innerHTML = kwArgs.html;
                }
                var parent = kwArgs.parentNode || kwArgs.parent;
                if (parent) {
                    if (kwArgs.hasOwnProperty("order")) {
                        parent.insertBefore(dom, parent.children[kwArgs.order]);
                    } else {
                        parent.appendChild(dom);
                    }
                }
                if (kwArgs.tooltip) {
                    dom.setAttribute("title", kwArgs.tooltip);
                }
                if (kwArgs.click) {
                    dom.onclick = kwArgs.click;
                }
                //                                if(kwArgs.value) {
                //                                    //dom.setAttribute("value", kwArgs.value);
                //                                    dom.value=kwArgs.value;
                //                                }
                return dom;
            }
        },
        _oldCreate: function(type, className, parentNode, attrs) {
            var dom = document.createElement(type || "div");
            if (className) {
                dom.className = className;
            }
            if (attrs) {
                for (var a in attrs) {
                    dom.setAttribute(a, attrs[a]);
                }
            }
            if (parentNode) {
                parentNode.appendChild(dom);
            }

            return dom;
        }

    };

});