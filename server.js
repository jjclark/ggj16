var express = require('express');
var fs = require('fs');
var request = require('request');
var bodyParser = require('body-parser');

var app = express();
app.use(express.static('./client'));


// persist interface to json data
var persist = {

	updateScenario: function(id, data, userData) {

	}

};

app.set('view engine', 'jade');

app.get('/data', function(req, res){
	var data = fs.readFileSync('./data/data.json');
	res.type('json');
	res.send(data);
});
app.use(bodyParser.json())
app.post('/data', function(req, res){
	var result = req.body;

	// write the new data to the data
	var data =  JSON.parse(fs.readFileSync('./data/data.json'));
	for(var k in result) {
		if(result[k]) {
			data.scenarios[k]=result[k];
		}else {
			delete data.scenarios[k];
		}
	}

	fs.writeFileSync('./data/data.json', JSON.stringify(data));
	res.send(200);
});

app.use('/admin', function(req, res){
	res.render('admin', { message: "hello Jade!"});
});

app.get('/giph', function(req, res){
	request("http://api.giphy.com/v1/gifs/search?q=waiting&limit=25&api_key=dc6zaTOxFJmzC", function(err,r,body){
		if(err) {
			console.log(err)
		}
		res.type('json');
		res.send(body);
	})
})

var port = process.env.PORT||8080;
app.listen(port, function () {
  console.log('BRITUALS server listening on port '+port+'!');
});