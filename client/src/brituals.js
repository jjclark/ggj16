/*
   BRITUALS
   [random ragline]

	I'm terribly sorry but   
	Would you mind awfully if I?
	One bing bong and two bong bings

	RITE OF PASSAGE

*/

var giphs;
$.get({
	url: "https://api.giphy.com/v1/gifs/search?q=waiting&limit=25&api_key=dc6zaTOxFJmzC",
	success:function(d){
		console.log("Preloaded "+d.data.length+" delicious giphy gifs");
		giphs=d.data;
	}
});

define(['src/util/domUtils'], function(dom){

	var environments=["surreal", "normal", "aggressive"];

	function rand(max){
		max=max||10;

		return Math.floor(Math.random()*max);
	}

	// thanks internet <3 http://jsfromhell.com/array/shuffle
	function shuffle(o){
	    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	    return o;
	}

	var databackup;

	var intermissions=[
		"The world spins on its axis a while, revelling in its own ennui.",
		"Time passes with a vague air of disapproval.",
		"Moments arrive in single file, think better of it, and slink away again.",
		"Unforgiving minutes ebb inexorably away.",
		"The bus driver of time pulls up to the stop, grumbles angrily at the commuters, and rolls off into eternity.",
		"Birds pass by overhead, migrating for to shores where disappointment seldom visits.",
		"Pages turn, clocks tick, students miss deadlines: life ambles on.",
		"Eldritch horrors abound in this strange place, each fleeting moment packed with awkward dread"
	];


	var BritualsGame = function(canvasNode, opts){
		this.nodes={
			canvas: canvasNode
		};
		this.initState();
		this.startup();
	};

	BritualsGame.prototype={

		turnLimit: 3,

		showSplash: true,

		initState: function(){
			var prev=localStorage.getItem("britual.env");
			var env;
			while(!env||env==prev){
				env = environments[rand(environments.length)];
			}
			localStorage.setItem("britual.env", env);

			this.state={
				turn: -1,
				done:{},
				env: env,
				score:0
			};
		},

		reset: function(){
			this.initState();
			this._initData(JSON.parse(databackup));
			this.step();
		},

		_loadRandom: function(){
			this.render(this._getScenario());
		},

		// todo 
		_showIntermission:function(){
			this.render(this._getIntermission());
		},

		startup: function(){
			
			console.log("Starting new game in the "+this.state.env+" Universe");
			this.loadGameData(function(data){
				this._initData(data);
				this.step();
			}.bind(this));
		},

		_initData: function(data){
			databackup = JSON.stringify(data);
			// flatten the prologue levels into an array
			var keys = Object.keys(data.scenarios);
			data.scenarios.prologue=[];
			var universeType=this.state.env;
			for(var i=0;i<keys.length;i++) {
				var item = data.scenarios[keys[i]];
				var isPrologue=false;
				if(keys[i].match(/prologue./)) {
					data.scenarios.prologue.push(item);
					delete data.scenarios[keys[i]];
					isPrologue=true;
				}

				// we also need to wrangle the targets to ignore anything that doesn't match this universe
				if(item && item.resolutions){
					var replace={};
					for(var e=0;e<item.resolutions.length;e++){
						var res = item.resolutions[e];
						if(res.env=="all"||item.resolutions[e].env==universeType){
							replace[item.resolutions[e].result]=item.resolutions[e];
						}
					}
					item.resolutions=replace;
				}
			}
			
			this.gameData = data;
		},

		// load game data
		loadGameData: function(done){
			$.getJSON('data.json', done)
		},

		// render a page on screen
		render: function(data){
			if(typeof data=="string"){
				// if we get a string, it's just a page resoltion.
				data = {
					content:data,
					options:[{
						content: "Continue..."
					}]
				}
			}
			var node = this.nodes.canvas;
			this.currentPage=data;
			node.innerHTML="";

			if(data.title) {
				dom.create({type:'h2', parent:node,text:data.title})
			}

			if(data.intermission) {
				// build the page
				node.appendChild(this._renderIntermission(node, data.content));
			}else {
				node.appendChild(this._renderScenario(node, data.content, data.options, data.image, data.finale));
			}

			// load options
		},

		_renderScenario: function(node, content, options, image, isFinale){
			var wrapper = dom.create({class:'scenarioWrapper'});
			// todo arrange this better

			var body = dom.create({parent:wrapper, class:'scenario'});

			if(image) {
				var imgWrapper = dom.create({class:'image-wrapper',parent:body})
				var img = dom.create({type:'img',parent:imgWrapper})
				img.setAttribute('src', 'img/'+image+".png")
			}
			dom.create({parent:body, html:content, class:'scenario-content'});
			

			var optionRoot = dom.create({type:'ul', parent:body, class:'options'});
			var state = this.state;
			var self=this;
			if(options){
				// shuffle the options array a bit
				options = shuffle(options);
				options.forEach(function(d){
					var n = dom.create({type:'li',parent:optionRoot, html:d.content, click:function(){
						var option = this.getAttribute('data-opt')
						self.step(option)
					}})
					if(d.results) {
						var universeType=state.env;
						n.setAttribute('data-opt', d.results[universeType]);
					}
				});
			}else if(!state.finished){
				dom.create({type:'li',parent:optionRoot, text:"Continue...", click:function(){
					self.step();
				}})
			}else {
				dom.create({type:'li',parent:optionRoot, text:"[Restart]", click:function(){
					self.reset();
				}})
			}
			return wrapper;
		},

		_renderIntermission: function(node, content){
			var wrapper = dom.create({class:"intermission"});
			var body = dom.create({parent:wrapper, html:content});
			var continueRoot = dom.create({type:'ul', parent:body});
			// add some kind of loading throbber
			var self=this;
			var giph = dom.create({class:'loading', parent:continueRoot});
			dom.create({type:'li',parent:continueRoot, text:"Continue...", click:function(){
				self.step()
			}})
			// load a random giphy gif

			if(giphs && giphs.length>0) {
				var ran = Math.floor(Math.random()*giphs.length)
				var imgNode = dom.create({type:"img"});
				var data = giphs[ran].images.fixed_height;
				imgNode.setAttribute("src",data.url)
				giph.appendChild(imgNode);
			}
			return wrapper;

		},


		// step through the game's ticks
		// this is the main game loop
		// turn one is the prologue, the last turn is the epilogue
		// every other turn loads a random scenario
		// each turn has a setup and a resolution
		step: function(option){
			var state = this.state;
			if(option) {
				// same turn, just figure out the resolution
				console.log("Completing turn"+state.turn+" with result: "+option);
				if(option=="undefined") {
					option="bad"; // default to "bad". Not because I'm evil, but because the CMSform defaults to bad and the value may be unset
				}
				var nextData = this.currentPage.resolutions[option];
				nextData.title = this.currentPage.title;
				this.updateScore(option);
				this.render(nextData);
			}else {

				if(this.hasPrologue()){
					this.render(this._getPrologue());
				}else {
					// render an intermission, then a scenario
					if(state.intermission){
						state.intermission = false;
						// load the next turn
						state.turn++;
						
						console.log("Beginning turn "+state.turn)
						var scenario = this._getScenario();
						if(!scenario||state.turn>=this.turnLimit) {
							scenario = this._getFinale();
						}
						this.render(scenario);
					}else {
						state.intermission = true;
						this.render(this._getIntermission());
					}
				}
			}

		},

		updateScore: function(result){
			var state = this.state;

			if(state.turn>=0){
				var modifier=0;
				if(!result || result=="bad") {
					modifier=-1;
				}else if(result=="good"){
					modifier=1
				}
				console.log("Score modifier: "+modifier);
				state.score+=modifier;
			}
		},

		hasPrologue: function(){
			return this.gameData.scenarios.prologue.length>0;
		},

		// we may have multiple prologues. Consider a shift() to get rid of them
		_getPrologue: function(){
			return this.gameData.scenarios.prologue.shift();
		},

		_getFinale:function(){
			var score=this.state.score;
			this.state.finished = true;
			var result="ok";
			if(score<0) {
				result="bad"
			}else if(score>=this.turns-1){
				result="good";
			}
			console.log("FINAL SCORE: "+score+" - show "+result.toUpperCase()+" ending");

			var base = this.gameData.scenarios.epilogue;
			// generate an option
			var option={
				content: "Proceed...",
				results: {
					normal: result,
					aggressive: result,
					surreal: result
				}
			}
			base.options=[option]
			return base;
		},

		loadScenario: function(id){
			var data = this.gameData.scenarios;
			if(data[id]){
				this.render(data[id]);
			}
		},

		_getScenario: function(){
			var data = this.gameData.scenarios;
			var keys = Object.keys(data);
			var state = this.state;
			//clear any specials
			for(var i=0;i<keys.length;i){
				if(keys[i].match(/prologue|epilogue/)||state.done[keys[i]]){
					keys.splice(i,1);
				}else {
					i++;
				}
			}
			var idx= Math.floor(Math.random()*keys.length);
			var result = data[keys[idx]];
			if(result) {
				state.done[result.id]=true;
				return result;
			}else {
				alert("All done! there are no more pags")
			}

		},

		_getIntermission:function(){
			return {
				"title": "---INTERMISSION---",
				content: intermissions[rand(intermissions.length)],
				intermission: true
			}
		},

		_getRandomThing: function(collection) {
			var item,index;
			

			return {
				item: item,
				idx: index
			}
		},

		generateCredits: function  (){
			var heroes = [
				["Joe", "Code", "jjc_uk"],
				["Damon", "Writing", "damonwakes"],
				["Sarah", "Writing", "irny"],
				["Clara", "Art & Styling", "claraalder"]
			];

			heroes = shuffle(heroes);
			heroes.push(["Kylie", "A Wee Bit of Art", "shadowtajun"]);

			// TODO - render
			//<div class="credit"><a class="name" href="http://www.twitter.com/jjc_uk" target="none">Joe</a>|<span class="role">Code</span></div>
			var root=$('#credits')[0];
			for(var i=0;i<heroes.length;i++) {
				var x = heroes[i];
				var c = dom.create({class:"credit", parent:root})
				var a = dom.create({type:'a', class: "name", parent:c,text:x[0]})
				a.setAttribute("href", "http://www.twitter.com/"+x[2]);
				a.setAttribute("target", "none");
				dom.create({type:'span', class: "role", text:" | "+x[1],parent:c})
			}

		}

	};

	return BritualsGame;


})
